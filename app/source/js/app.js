
/* takes array, return next elem
 var arr = [1,2,3,4,5];
 nextElement(arr);
 */

var nextElement = function(array){
    array.i = 0;
    array.next = function(){
        if(array.i > array.length-1){
            array.i = 0;
        }
        array.i++;
        return array[array.i-1];
    };
};

/* convert RGB to HEX */

$.cssHooks.backgroundColor = {
    get: function(elem) {
        'use strict';
        var bg;
        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        if (elem.currentStyle) {
            bg = elem.currentStyle["backgroundColor"];
        } else if (window.getComputedStyle) {
            bg = document.defaultView.getComputedStyle(elem,
                null).getPropertyValue("background-color");
        }
        if (bg.search('rgb') === -1) {
            return bg;
        } else {
            bg = bg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            hex(bg);
            return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
        }
    }
};

$(function() {
    var price_poncho = parseInt($("#price_poncho").text());
    var price_cardigan = parseInt($("#price_cardigan").text());
    var price_sleeve = parseInt($("#price_rukav").text());
    var price_neck = parseInt($("#price_neck").text());

    $(".type p").on('click', function(e){
        $(".type ul").hide();
        $(e.target).next('ul').show();
    });

    $(".close-btn").on("click", function(){
        //$(".how-it-work").slideUp();
        $(".how-it-work").css({"width": "0", "height": "0","padding": "0","text-indent":"-9999px"});
        setTimeout(function(){
            $(".show-intr").show();
        }, 350);
    });

    $(".show-intr").on("click", function(e){
        //$(".how-it-work").animate({'width':'100%', 'height':'100%',"padding-top": "135px"},'fast');
        $(".how-it-work").css({"text-indent": "inherit", 'width':'100%', 'height':'100%',"padding-top": "135px"});
        //$(".how-it-work").slideDown();
        $(e.target).hide();
    });

    'use strict';
    var w = 355;
    var h = 768;
    var front = new Raphael("front");
    front.setViewBox(0,0,w,h,true);

    var wb = 232;
    var hb = 366;
    //var hb = 266;
    var back = new Raphael("back");
    back.setViewBox(0,0,wb,hb,true);

    var currentPart = {};
    var robe = {};

    var fontStyle = {
        "font-size":"18px",
        "fill": "#bfbfbf"
    };

    var defaultPartBackground = {
        "fill":'#fff',
        "stroke": "#bfbfbf"
    };

    var defaultStrokeStyle = {
        "stroke": "#bfbfbf"
    };

    var config = {
        type: 'kardigan', /* poncho */
        sleeve: 'threeQuarterStraight', /* longStraight, longRounded, threeQuarterStraight */
        bottom: 'straight', /* rounded */
        neck: 'without', /* with */
        dirtyBitch: false
    };
    $("#order-info").val($.param(config));

    var kardiganFace = [
        //sleeve_top
        "M103,155C103,129,103,70,77,63L32,177L87,203ZM276,63C257,72,251,110,251,155L269,208L326,188Z",
        //sleeve_bottom long straight
        "M32,177L87,203L61,286L0,261ZM269,208L326,188L354,261L292,286Z",
        // рукав 3/4 звичайний
        "M32,177L87,203L80,223L25,195Z M269,208L326,188L333,206L275,229Z",
        // рукав 3/4 круглий
        "M32,177L87,203C79,229 79,229 50.5, 217.5 C22,206 22,206 32,177ZM53.219,205.686c0,0,12.979,6.091,11.086,11.255c-1.894,5.165-13.372,0.416-15.709-0.598c-2.337-1.016-12.939-5.386-10.836-11.032C39.863,199.661,53.219,205.686,53.219,205.686zM269,208L326,188C333,206 333,206 304, 217.5 C275,229 275,229 269,208ZM301.324,205.006c0,0,13.573-4.615,15.793,0.417c2.218,5.034-9.345,9.574-11.731,10.466c-2.388,0.89-13.056,5.095-15.458-0.431C287.525,209.93,301.324,205.006,301.324,205.006z",
        // рукав круглий довгий
        "M32,177L7,236C0,266,21,271,27,274C53,283,62,281,70,255L87,203L32,177M29.576,273.271c0,0-14.103-5.679-12.345-11.437c1.756-5.758,14.198-1.429,16.732-0.497c2.537,0.933,14.026,4.912,12.07,11.211C44.075,278.848,29.576,273.271,29.576,273.271zM269,208L286,260C300,292,329,274,329,274C338,268,353,270,350,241L326,188L269,208M326.054,272.963c0,0-14.262,5.27-16.828-0.176  c-2.567-5.444,9.556-10.598,12.06-11.61c2.505-1.014,13.688-5.786,16.472,0.194C340.54,267.353,326.054,272.963,326.054,272.963z",
        //body_top with neck
        "M177,160L177,218L103,218C103,129,103,70,77,63C90,32,88,38,118,35L118,45C120 50 118,56 145,82C165,109 177,120 177,160C177,119,189,107,209,82C236,56,234,50,236,45L236,35C274,40 260,34 276,63C257,72 251,110 251, 155L251,218L177,218",
        // звичайний низ
        "M177,218L177,360L103,360L103,218L251,218L251,360L177,360",
        "M177,360L177,414L103,414L103,360L251,360L251,414L177,414",
        // заокруглений низ
        "M177,217L177,309C177,388 103,388 103,388L103,388L103,218L251,218L251,388C251,388 177,388 177,309 L177,309",
        "M177,309C177,360,155,414,103,414L103,388C103,388 177,388 177,309Z M177,309C177,388 251,388 251,388L251,414C251,414,177,414,177,309Z M177,309C177,360,155,414,105,414L249,414C251,414,177,414,177,309Z",
        //neck
        "M145,6C143,6,143,23,151,32L202,32C211,20,212,5,209,5C206,0,145,0,145,6C139,15,152,33,152,33C181,60,177,102,177,160C177,120,165,109,145,82C118,56,120,50,118,45C118,11,132,11,145,6C145,0,213,0,210,6C215,15,202,33,202,33C174,57,177,100,177,160C177,119,189,107,209,82C236,56,234,50,236,45C236,11,220,11,213,6C213,0,145,0,145,6",
        //body_top without neck
        "M177,95L177,218L103,218C103,129,103,70,77,63C90,32,88,38,137,35L177,95L218,35C274,40,260,34,276,63C257,72,251,110,251,155L251,218L177,218"
    ];

    var kardiganBack = [
        //back_body_top
        "M52,37 C62,20 62,18 72,18L78,18L160,18C170,20 170,18 180,37C165,37 165,73 165,98L165,136L67,136L67,98C67,73 67,37 52,37Z",
        //back_body_middle
        "M67,136L165,136,L165,229L67,229Z",
        //back_body_bottom
        "M67,229L165,229,L165,266L67,266Z",
        // рукав верх
        "M52,37C67,37,67,73,67,98L59,118L25,105Z M165,98C165,73,165,37,180,37L207,105L173,118Z",
        // довгий рукав
        "M25,105L59,118L40,183L0,167Z M173,118L207,105L232,167L192,183Z",
        // рукав 3/4
        "M25,105L59,118 L55,131 L20,118Z M173,118L207,105 L212,118L177,131Z",
        // круглий довгий рукав
        "M25,105L59,118C40,183 40,183 20,175C0,167 0,167 24,107Z M173,118L207,105C232,167 232,167 212,175C192,183 192,183 173,118Z",
        // рукав 3/4 круглий
        "M24,107L60,115C51,143 51,143 33.5 135C16,127 16,127 24,107ZM172,115L208,107C216,127 216,127 198.5,135C181,143 181,143 172,115Z",
        //back_neck
        "M78,18c0,0,5.5-13.666,24-18h26.835c0,0,19.834,3.5,24.5,18z"
    ];

    var ponchoFace = [
        // top
        "M255.454,74.281 C250.863,74.45700000000001,246.70600000000002,79.23,243.69,86.819 C240.607,94.54,238.71099999999998,105.186,238.71099999999998,116.95400000000001 C238.71099999999998,128.70000000000002,240.60699999999997,139.347,243.69,147.08800000000002 C246.728,154.699,250.863,159.45100000000002,255.454,159.62600000000003V152.01500000000004 C252.279,149.97100000000003,249.538,145.35100000000006,247.597,139.12600000000003 C245.70100000000002,133.01100000000002,244.559,125.31200000000004,244.559,116.95400000000004 C244.559,108.59500000000004,245.702,100.91800000000003,247.597,94.78200000000004 C249.538,88.55700000000004,252.28,83.93700000000004,255.454,81.89200000000004V74.281Z  M100.315,73.946C104.717,74.122,108.701,78.88499999999999,111.592,86.459C114.548,94.165,116.365,104.789,116.365,116.534C116.365,128.257,114.547,138.882,111.592,146.61 C108.679,154.204,104.717,158.94600000000003,100.315,159.12300000000002V151.52800000000002 C103.359,149.48600000000002,105.986,144.87600000000003,107.848,138.663 C109.666,132.56,110.761,124.876,110.761,116.534C110.761,108.19200000000001,109.666,100.53200000000001,107.848,94.406 C105.987,88.19300000000001,103.359,83.583,100.315,81.542V73.946Z  M177,32L177,218L100,218L100,159 C120,161,122,75,100,74 L100,35 L160,19 C166,35,186,36,194,19 L256,35L256,75 C229,75,237,160,256,160 L256,218L177,218",
        // bottom straight
        "M177,218L177,370L100,370L100,218L177,218L256,218L256,370L177,370",
        "M177,370L177,414L100,414L100,370L177,370L256,370,256,414L177,414",
        // bottom round
        "M177,217L177,309C177,388 100,388 100,388L100,388L100,218L256,218L256,388C256,388 177,388 177,309 L177,309",
        "M177,309C177,360,155,414,100,414 L100,388C100,388 177,388 177,309Z M177,309C177,388 256,388 256,388L256,414C256,414,177,414,177,309Z M177,309C177,360,155,414,102,414L254,414C256,414,177,414,177,309"
    ];
    var ponchoBack = [
        // body top
        "M64.143,40.176C67.09,40.295,69.76,43.517,71.695,48.641000000000005C73.675,53.854000000000006,74.89399999999999,61.041000000000004,74.89399999999999,68.98700000000001C74.89399999999999,76.917,73.675,84.105,71.695,89.33300000000001C69.74499999999999,94.47100000000002,67.08999999999999,97.67900000000002,64.143,97.79800000000002V92.66C66.182,91.27799999999999,67.941,88.16,69.187,83.957C70.405,79.82799999999999,71.139,74.63,71.139,68.987C71.139,63.342999999999996,70.40599999999999,58.16,69.187,54.016999999999996C67.941,49.81399999999999,66.181,46.69499999999999,64.143,45.31399999999999V40.176Z M168.812,40.413C165.857,40.531,163.17700000000002,43.733,161.233,48.824C159.248,54.004,158.025,61.144999999999996,158.025,69.03999999999999C158.025,76.919,159.24800000000002,84.06099999999999,161.233,89.255C163.192,94.36099999999999,165.857,97.548,168.812,97.666V92.56C166.768,91.188,165.002,88.089,163.751,83.913C162.528,79.81099999999999,161.792,74.646,161.792,69.039C161.792,63.431000000000004,162.528,58.282000000000004,163.751,54.164C165.002,49.988,166.768,46.889,168.812,45.517V40.413Z M64,17L101,6L132,6L169,17L169,40C154,40,154,97,169,97L169,141L64,141L64,97C79,97,79,40,64,40Z",
        // body middle
        "M64,141L169,141L169,234L64,233Z",
        // body bottom
        "M64,233L169,233L169,233L169,267L64,267Z"
    ];

// kardigan
    front.set().push(
        front.path("M0,0L356, 0L356,800L0,800Z").attr({"fill":"#ffffff", "stroke":"#fff"})
    );
    robe.sleeve_top = front.set().push(
        /* top */
        front.path(kardiganFace[0]).attr(defaultPartBackground),
        front.path(kardiganFace[0]).attr(defaultStrokeStyle),
        back.path(kardiganBack[3]).attr(defaultPartBackground),
        back.path(kardiganBack[3]).attr(defaultStrokeStyle),
        front.text(77, 148, '3').attr(fontStyle),
        front.text(283, 148, '4').attr(fontStyle),
        back.text(180, 83, '3').attr(fontStyle),
        back.text(53, 83, '4').attr(fontStyle)
    );


    robe.sleeve_bottom = front.set().push(
        // рукав 3/4
        front.path(kardiganFace[2]).attr(defaultPartBackground),
        front.path(kardiganFace[2]).attr(defaultStrokeStyle),
        back.path(kardiganBack[5]).attr(defaultPartBackground),
        back.path(kardiganBack[5]).attr(defaultStrokeStyle),
        front.text(60, 202, '5').attr(fontStyle),
        front.text(297, 208, '6').attr(fontStyle),
        back.text(193, 118, '5').attr(fontStyle),
        back.text(42, 118, '6').attr(fontStyle)
    );

    robe.body_top = front.set().push(
        front.path(kardiganFace[11]).attr(defaultPartBackground),
        front.path(kardiganFace[11]).attr(defaultStrokeStyle),
        back.path(kardiganBack[0]).attr(defaultPartBackground),
        back.path(kardiganBack[0]).attr(defaultStrokeStyle),
        front.image("http://knitel.com.ua/img/constructor/model-front-head-withoutneck.png", 137, -74, 80, 169),
        front.text(135, 158, '2').attr(fontStyle),
        back.text(115, 70, '2').attr(fontStyle)
    );

// звичайний низ
    robe.body_middle = front.set().push(
        front.path(kardiganFace[6]).attr(defaultPartBackground),
        front.path(kardiganFace[6]).attr(defaultStrokeStyle),
        back.path(kardiganBack[1]).attr(defaultPartBackground),
        back.path(kardiganBack[1]).attr(defaultStrokeStyle),
        front.text(135, 269, '7').attr(fontStyle),
        back.text(115, 189, '7').attr(fontStyle)
    );
    robe.body_bottom = front.set().push(
        front.path(kardiganFace[7]).attr(defaultPartBackground),
        front.path(kardiganFace[7]).attr(defaultStrokeStyle),
        back.path(kardiganBack[2]).attr(defaultPartBackground),
        back.path(kardiganBack[2]).attr(defaultStrokeStyle),
        front.image("/img/constructor/model-front-legs-short.png", 124, 414, 111, 272),
        front.text(135, 378, '8').attr(fontStyle),
        back.text(115, 250, '8').attr(fontStyle)
    );
//neck
//    robe.neck = front.set().push(
        //front.path(kardiganFace[10]).attr(defaultPartBackground),
        //front.path(kardiganFace[10]).attr(defaultStrokeStyle),
        //back.path(kardiganBack[8]).attr(defaultPartBackground),
        //back.path(kardiganBack[8]).attr(defaultStrokeStyle),
        //front.image("http://knitel.com.ua/img/constructor/model-front-head.png", 137, -74, 80, 169),
        //front.text(147, 60, '1').attr(fontStyle)
    //);

    var attachEvents = function(part){
        part.mouseover(function() {
            for(var i in robe){
                if (robe[i].state == 0) {
                    robe[i].attr(defaultStrokeStyle);
                }
                if( _.isEqual(robe[i], part)) {
                    part[1].animate({stroke: "#f00"}, 100);
                    part[3].animate({stroke: "#f00"}, 100);
                }
            }

            //part[1].animate({stroke: "#f00"}, 100);
            //if (part[3]) {part[3].animate({stroke: "#f00"}, 100);}
            //part.toFront();
        });
        part.mouseout(function() {
            if (_.isEqual(part, currentPart)) {
                return;
            }
            if(this.state === 0){
                part[1].animate({stroke: "#f00"}, 100);
                if (part[3]) {part[3].animate({stroke: "#f00"}, 100);}
            } else {
                part[1].animate(defaultStrokeStyle, 100);
                if (part[3]) {
                    part[3].animate(defaultStrokeStyle, 100);
                    //part.toBack();
                }
            }
        });
        part.click(function() {
            if(part.state === 0) {
                for(var i in robe){
                    robe[i].state = 0;
                    robe[i].attr(defaultStrokeStyle);
                }
                part.state = 1;
                part[1].attr({stroke: "#f00"});
                if (part[3]) {part[3].animate({stroke: "#f00"}, 100);}
                currentPart = part;
                part.toFront();
            } else {
                part.state = 0;
                part[1].attr(defaultStrokeStyle);
                if (part[3]) {part[3].animate(defaultStrokeStyle, 100);}
                currentPart = {};
            }
        });
    };

    var iterateCollection = function(){
        for(var i in robe) {
            var part = robe[i];
            part.state = 0;
            attachEvents(part);
        }
    };

    iterateCollection();

    var transform = function(){
        for(var i in robe) {
            var part = robe[i];
            part.transform("t0,83");
        }
    };

    transform();

    /* select pattern */
    $('#patterns').on('click', function(e){
        if (currentPart.length) {
            config.dirtyBitch = true;
            currentPart[1].animate({fill: $(e.target).next().css('background-image')}, 100);
            if (currentPart[3]) {currentPart[3].animate({fill: $(e.target).next().css('background-image')}, 100);}
        }
    });
    /* select color */
    $('#colors').on('click touchstart', function(e){
        if (currentPart.length && $(e.target).hasClass('color')) {
            config.dirtyBitch = true;
            currentPart[0].animate({fill: $(e.target).css('background-color')}, 1000);
            if (currentPart[2]) {currentPart[2].animate({fill: $(e.target).css('background-color')}, 1000);}
        }
    });

    $('#with-neck').on('click', function(){
        if (config.sleeve == "longStraight" || config.sleeve == "longRounded") {
            $("#order-price").text(price_cardigan+price_sleeve+price_neck);
        } else {
            $("#order-price").text(price_cardigan+price_neck);
        }
        if (robe.neck) {
            robe.neck.forEach(function(el) {el.remove();});
        }
        robe.body_top.forEach(function(el) {el.remove();});
        robe.body_top = front.set().push(
            front.path(kardiganFace[5]).attr(defaultPartBackground),
            front.path(kardiganFace[5]).attr(defaultStrokeStyle),
            back.path(kardiganBack[0]).attr(defaultPartBackground),
            back.path(kardiganBack[0]).attr(defaultStrokeStyle),
            front.text(135, 158, '2').attr(fontStyle),
            back.text(115, 70, '2').attr(fontStyle)
        );
        robe.neck = front.set().push(
            front.path(kardiganFace[10]).attr(defaultPartBackground),
            front.path(kardiganFace[10]).attr(defaultStrokeStyle),
            back.path(kardiganBack[8]).attr(defaultPartBackground),
            back.path(kardiganBack[8]).attr(defaultStrokeStyle),
            front.image("http://knitel.com.ua/img/constructor/model-front-head.png", 137, -74, 80, 169),
            front.text(147, 60, '1').attr(fontStyle),
            back.text(115, 9, '1').attr(fontStyle)
        );
        config.neck = 'with';
        robe.neck.state = 0;
        robe.body_top.state = 0;
        transform();
        attachEvents(robe.neck);
        attachEvents(robe.body_top);
    });
    $('#without-neck').on('click', function(){
        if (config.sleeve == "longStraight" || config.sleeve == "longRounded") {
            $("#order-price").text(price_cardigan+price_sleeve);
        } else {
            $("#order-price").text(price_cardigan);
        }

        robe.body_top.forEach(function(el) {el.remove();});
        if (robe.neck) {
            robe.neck.forEach(function(el) {el.remove();});
        }
        robe.body_top = front.set().push(
            front.path(kardiganFace[11]).attr(defaultPartBackground),
            front.path(kardiganFace[11]).attr(defaultStrokeStyle),
            back.path(kardiganBack[0]).attr(defaultPartBackground),
            back.path(kardiganBack[0]).attr(defaultStrokeStyle),
            front.image("http://knitel.com.ua/img/constructor/model-front-head-withoutneck.png", 137, -74, 80, 169),
            front.text(135, 158, '2').attr(fontStyle),
            back.text(115, 70, '2').attr(fontStyle)
        );
        config.neck = 'without';
        robe.body_top.state = 0;
        if (robe.neck) {
            robe.neck.state = 0;
        }
        transform();
        attachEvents(robe.body_top);
        attachEvents(robe.neck);
    });
    $('#type-sleeve-1').on('click', function(){
        if (config.neck == "with") {
            $("#order-price").text(price_cardigan+price_neck+price_sleeve);
        } else {
            $("#order-price").text(price_cardigan+price_sleeve);
        }
        switch (config.sleeve) {
            case 'longRounded':
                robe.sleeve_bottom.forEach(function(el) {el.remove();});
                robe.sleeve_bottom = front.set().push(
                    front.path(kardiganFace[1]).attr(defaultPartBackground),
                    front.path(kardiganFace[1]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[4]).attr(defaultPartBackground),
                    back.path(kardiganBack[4]).attr(defaultStrokeStyle),
                    front.text(40, 233, '5').attr(fontStyle),
                    front.text(311, 245, '6').attr(fontStyle),
                    back.text(197, 135, '5').attr(fontStyle),
                    back.text(35, 135, '6').attr(fontStyle)
                );
                config.sleeve = 'longStraight';
                robe.sleeve_bottom.state = 0;
                transform();
                attachEvents(robe.sleeve_bottom);
                break;
            case 'threeQuarterRounded':
                robe.sleeve_bottom.forEach(function(el) {el.remove();});
                robe.sleeve_bottom = front.set().push(
                    front.path(kardiganFace[1]).attr(defaultPartBackground), // 2
                    front.path(kardiganFace[1]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[4]).attr(defaultPartBackground),
                    back.path(kardiganBack[4]).attr(defaultStrokeStyle),
                    front.text(40, 233, '5').attr(fontStyle),
                    front.text(311, 245, '6').attr(fontStyle),
                    back.text(197, 135, '5').attr(fontStyle),
                    back.text(35, 135, '6').attr(fontStyle)
                );
                config.sleeve = 'threeQuarterStraight';
                robe.sleeve_bottom.state = 0;
                transform();
                attachEvents(robe.sleeve_bottom);
                break;
            case 'threeQuarterStraight':
                robe.sleeve_bottom.forEach(function(el) {el.remove();});
                robe.sleeve_bottom = front.set().push(
                    front.path(kardiganFace[1]).attr(defaultPartBackground),
                    front.path(kardiganFace[1]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[4]).attr(defaultPartBackground),
                    back.path(kardiganBack[4]).attr(defaultStrokeStyle),
                    front.text(40, 233, '5').attr(fontStyle),
                    front.text(311, 245, '6').attr(fontStyle),
                    back.text(197, 135, '5').attr(fontStyle),
                    back.text(35, 135, '6').attr(fontStyle)
                );
                config.sleeve = 'longStraight';
                robe.sleeve_bottom.state = 0;
                transform();
                attachEvents(robe.sleeve_bottom);
                break;
        }
    });
    $('#type-sleeve-2').on('click', function(){
        if (config.neck == "with") {
            $("#order-price").text(price_cardigan+price_neck+price_sleeve);
        } else {
            $("#order-price").text(price_cardigan+price_sleeve);
        }
        robe.sleeve_bottom.forEach(function(el) {el.remove();});
        robe.sleeve_bottom = front.set().push(
            front.path(kardiganFace[4]).attr(defaultPartBackground),
            front.path(kardiganFace[4]).attr(defaultStrokeStyle),
            back.path(kardiganBack[6]).attr(defaultPartBackground),
            back.path(kardiganBack[6]).attr(defaultStrokeStyle),
            front.text(40, 233, '5').attr(fontStyle),
            front.text(311, 245, '6').attr(fontStyle),
            back.text(197, 135, '5').attr(fontStyle),
            back.text(35, 135, '6').attr(fontStyle)
        );
        config.sleeve = 'longRounded';
        robe.sleeve_bottom.state = 0;
        transform();
        attachEvents(robe.sleeve_bottom);
    });
    $('#type-sleeve-3').on('click', function(){
        if (config.neck == "with") {
            $("#order-price").text(price_cardigan+price_neck);
        } else {
            $("#order-price").text(price_cardigan);
        }
        switch (config.sleeve) {
            case 'longStraight':
                robe.sleeve_bottom.forEach(function(el) {el.remove();});
                robe.sleeve_bottom = front.set().push(
                    front.path(kardiganFace[2]).attr(defaultPartBackground),
                    front.path(kardiganFace[2]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[5]).attr(defaultPartBackground),
                    back.path(kardiganBack[5]).attr(defaultStrokeStyle),
                    front.text(60, 202, '5').attr(fontStyle),
                    front.text(297, 208, '6').attr(fontStyle),
                    back.text(193, 118, '5').attr(fontStyle),
                    back.text(42, 118, '6').attr(fontStyle)
                );
                config.sleeve = 'threeQuarterStraight';
                robe.sleeve_bottom.state = 0;
                transform();
                attachEvents(robe.sleeve_bottom);
                break;
            case 'longRounded':
                robe.sleeve_bottom.forEach(function(el) {el.remove();});
                robe.sleeve_bottom = front.set().push(
                    front.path(kardiganFace[2]).attr(defaultPartBackground), // 3
                    front.path(kardiganFace[2]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[5]).attr(defaultPartBackground),
                    back.path(kardiganBack[5]).attr(defaultStrokeStyle),
                    front.text(60, 202, '5').attr(fontStyle),
                    front.text(297, 208, '6').attr(fontStyle),
                    back.text(193, 118, '5').attr(fontStyle),
                    back.text(42, 118, '6').attr(fontStyle)
                );
                config.sleeve = 'threeQuarterRounded';
                robe.sleeve_bottom.state = 0;
                transform();
                attachEvents(robe.sleeve_bottom);
                break;
            case 'threeQuarterStraight':
                robe.sleeve_bottom.forEach(function(el) {el.remove();});
                robe.sleeve_bottom = front.set().push(
                    front.path(kardiganFace[2]).attr(defaultPartBackground), // 1
                    front.path(kardiganFace[2]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[5]).attr(defaultPartBackground), // 4
                    back.path(kardiganBack[5]).attr(defaultStrokeStyle),
                    front.text(60, 202, '5').attr(fontStyle),
                    front.text(297, 208, '6').attr(fontStyle),
                    back.text(193, 118, '5').attr(fontStyle),
                    back.text(42, 118, '6').attr(fontStyle)
                );
                config.sleeve = 'longStraight';
                robe.sleeve_bottom.state = 0;
                transform();
                attachEvents(robe.sleeve_bottom);
                break;
        }
    });
    $('#type-bottom-1').on('click', function(){
        switch (config.type) {
            case 'kardigan':
                //set rounded bottom
                config.bottom = 'rounded';
                robe.body_middle.forEach(function(el) {el.remove();});
                robe.body_bottom.forEach(function(el) {el.remove();});
                robe.body_middle = front.set().push(
                    front.path(kardiganFace[8]).attr(defaultPartBackground),
                    front.path(kardiganFace[8]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[1]).attr(defaultPartBackground),
                    back.path(kardiganBack[1]).attr(defaultStrokeStyle),
                    front.text(135, 269, '7').attr(fontStyle),
                    back.text(115, 189, '7').attr(fontStyle)
                );
                robe.body_bottom = front.set().push(
                    front.path(kardiganFace[9]).attr(defaultPartBackground),
                    front.path(kardiganFace[9]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[2]).attr(defaultPartBackground),
                    back.path(kardiganBack[2]).attr(defaultStrokeStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-legs.png", 124, 383, 111, 303),
                    front.text(135, 390, '8').attr(fontStyle),
                    back.text(115, 250, '8').attr(fontStyle)
                );
                robe.body_middle.state = 0;
                robe.body_bottom.state = 0;
                transform();
                attachEvents(robe.body_middle);
                attachEvents(robe.body_bottom);
                break;
            case 'poncho':
                //set rounded bottom
                config.bottom = 'rounded';
                robe.body_middle.forEach(function(el) {el.remove();});
                robe.body_bottom.forEach(function(el) {el.remove();});
                robe.body_middle = front.set().push(
                    front.path(ponchoFace[3]).attr(defaultPartBackground),
                    front.path(ponchoFace[3]).attr(defaultStrokeStyle),
                    back.path(ponchoBack[1]).attr(defaultPartBackground),
                    back.path(ponchoBack[1]).attr(defaultStrokeStyle),
                    front.text(135, 269, '2').attr(fontStyle),
                    back.text(115, 189, '2').attr(fontStyle)
                );
                robe.body_bottom = front.set().push(
                    front.path(ponchoFace[4]).attr(defaultPartBackground),
                    front.path(ponchoFace[4]).attr(defaultStrokeStyle),
                    back.path(ponchoBack[2]).attr(defaultPartBackground),
                    back.path(ponchoBack[2]).attr(defaultStrokeStyle),
                    front.text(135, 390, '3').attr(fontStyle),
                    back.text(115, 250, '3').attr(fontStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-legs.png", 124, 383, 111, 303)
                );
                robe.body_middle.state = 0;
                robe.body_bottom.state = 0;
                transform();
                attachEvents(robe.body_middle);
                attachEvents(robe.body_bottom);
        }
    });
    $('#type-bottom-2').on('click', function(){
        switch (config.type) {
            case 'kardigan':
                //ste straight bottom
                config.bottom = 'straight';
                robe.body_middle.forEach(function(el) {el.remove();});
                robe.body_bottom.forEach(function(el) {el.remove();});
                robe.body_middle = front.set().push(
                    front.path(kardiganFace[6]).attr(defaultPartBackground),
                    front.path(kardiganFace[6]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[1]).attr(defaultPartBackground),
                    back.path(kardiganBack[1]).attr(defaultStrokeStyle),
                    front.text(135, 269, '7').attr(fontStyle),
                    back.text(115, 189, '7').attr(fontStyle)
                );
                robe.body_bottom = front.set().push(
                    front.path(kardiganFace[7]).attr(defaultPartBackground),
                    front.path(kardiganFace[7]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[2]).attr(defaultPartBackground),
                    back.path(kardiganBack[2]).attr(defaultStrokeStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-legs-short.png", 124, 414, 111, 272),
                    front.text(135, 378, '8').attr(fontStyle),
                    back.text(115, 250, '8').attr(fontStyle)
                );
                robe.body_middle.state = 0;
                robe.body_bottom.state = 0;
                transform();
                attachEvents(robe.body_middle);
                attachEvents(robe.body_bottom);
                break;
            case 'poncho':
                //ste straight bottom
                config.bottom = 'straight';
                robe.body_middle.forEach(function(el) {el.remove();});
                robe.body_bottom.forEach(function(el) {el.remove();});
                robe.body_middle = front.set().push(
                    front.path(ponchoFace[1]).attr(defaultPartBackground),
                    front.path(ponchoFace[1]).attr(defaultStrokeStyle),
                    back.path(ponchoBack[1]).attr(defaultPartBackground),
                    back.path(ponchoBack[1]).attr(defaultStrokeStyle),
                    front.text(135, 269, '2').attr(fontStyle),
                    back.text(115, 189, '2').attr(fontStyle)
                );
                robe.body_bottom = front.set().push(
                    front.path(ponchoFace[2]).attr(defaultPartBackground),
                    front.path(ponchoFace[2]).attr(defaultStrokeStyle),
                    back.path(ponchoBack[2]).attr(defaultPartBackground),
                    back.path(ponchoBack[2]).attr(defaultStrokeStyle),
                    front.text(135, 390, '3').attr(fontStyle),
                    back.text(115, 250, '3').attr(fontStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-legs-short.png", 124, 414, 111, 272)
                );
                robe.body_middle.state = 0;
                robe.body_bottom.state = 0;
                transform();
                attachEvents(robe.body_middle);
                attachEvents(robe.body_bottom);
        }
    });


    $(".type-wear").on('click', function(){
        for(var i in robe) {
            var part = robe[i];
            part.forEach(function(el) {
                el.remove();
            });
        }
        $(".type-wear").toggleClass('wear-2');
        switch (config.type) {
            case 'kardigan':
                config.type = 'poncho';
                config.sleeve = null;
                $(".type-sleeve").hide();
                $(".type-neck").hide();
                $(".type-bottom ul").show();
                $(".poncho").toggleClass('hidden');
                $(".kardigan").toggleClass('hidden');
                $("#order-price").text($("#price_poncho").text());
                $("#order-type").text("poncho");
                //poncho
                robe.body_top = front.set().push(
                    front.path(ponchoFace[0]).attr(defaultPartBackground),
                    front.path(ponchoFace[0]).attr(defaultStrokeStyle),
                    back.path(ponchoBack[0]).attr(defaultPartBackground),
                    back.path(ponchoBack[0]).attr(defaultStrokeStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-head-poncho.png", 137, -74, 80, 106),
                    front.image("http://knitel.com.ua/img/constructor/front-arm-left.png", 2, 82, 111, 211),
                    front.image("http://knitel.com.ua/img/constructor/front-arm-right.png", 241, 82, 111, 211),
                    front.text(135, 158, '1').attr(fontStyle),
                    back.text(115, 70, '1').attr(fontStyle),
                    front.image("http://knitel.com.ua/img/constructor/front-arm-left.png", 362, 297, 70, 134),
                    front.image("http://knitel.com.ua/img/constructor/front-arm-right.png", 520, 297, 70, 134)

                );
                robe.body_middle = front.set().push(
                    front.path(ponchoFace[1]).attr(defaultPartBackground),
                    front.path(ponchoFace[1]).attr(defaultStrokeStyle),
                    back.path(ponchoBack[1]).attr(defaultPartBackground),
                    back.path(ponchoBack[1]).attr(defaultStrokeStyle),
                    front.text(135, 269, '2').attr(fontStyle),
                    back.text(115, 189, '2').attr(fontStyle)
                );
                robe.body_bottom = front.set().push(
                    front.path(ponchoFace[2]).attr(defaultPartBackground),
                    front.path(ponchoFace[2]).attr(defaultStrokeStyle),
                    back.path(ponchoBack[2]).attr(defaultPartBackground),
                    back.path(ponchoBack[2]).attr(defaultStrokeStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-legs-short.png", 124, 414, 111, 272),
                    front.text(135, 390, '3').attr(fontStyle),
                    back.text(115, 250, '3').attr(fontStyle)
                );

                transform();
                iterateCollection();
                break;

            case 'poncho':
                config.type = 'kardigan';
                config.sleeve = 'threeQuarterStraight';
                $(".type-sleeve").show();
                $(".type-neck").show();
                $(".type-neck ul").show();
                $(".type-bottom ul").hide();
                $(".poncho").toggleClass('hidden');
                $(".kardigan").toggleClass('hidden');
                $("#order-price").text($("#price_cardigan").text());
                $("#order-type").text("kardigan");
                // kardigan
                robe.sleeve_top = front.set().push(
                    /* top */
                    front.path(kardiganFace[0]).attr(defaultPartBackground),
                    front.path(kardiganFace[0]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[3]).attr(defaultPartBackground),
                    back.path(kardiganBack[3]).attr(defaultStrokeStyle),
                    front.text(77, 148, '3').attr(fontStyle),
                    front.text(283, 148, '4').attr(fontStyle),
                    back.text(180, 83, '3').attr(fontStyle),
                    back.text(53, 83, '4').attr(fontStyle)
                );


                robe.sleeve_bottom = front.set().push(
                    // рукав довгий звичайний
                    front.path(kardiganFace[2]).attr(defaultPartBackground),
                    front.path(kardiganFace[2]).attr(defaultStrokeStyle),
                    // довгий рукав
                    back.path(kardiganBack[5]).attr(defaultPartBackground),
                    back.path(kardiganBack[5]).attr(defaultStrokeStyle),

                    front.text(60, 202, '5').attr(fontStyle),
                    front.text(297, 208, '6').attr(fontStyle),
                    back.text(193, 118, '5').attr(fontStyle),
                    back.text(42, 118, '6').attr(fontStyle)
                );

                robe.body_top = front.set().push(
                    front.path(kardiganFace[11]).attr(defaultPartBackground),
                    front.path(kardiganFace[11]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[0]).attr(defaultPartBackground),
                    back.path(kardiganBack[0]).attr(defaultStrokeStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-head-withoutneck.png", 137, -74, 80, 169),
                    front.text(135, 158, '2').attr(fontStyle),
                    back.text(115, 70, '2').attr(fontStyle)
                );

                // звичайний низ
                robe.body_middle = front.set().push(
                    front.path(kardiganFace[6]).attr(defaultPartBackground),
                    front.path(kardiganFace[6]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[1]).attr(defaultPartBackground),
                    back.path(kardiganBack[1]).attr(defaultStrokeStyle),
                    front.text(135, 269, '7').attr(fontStyle),
                    back.text(115, 189, '7').attr(fontStyle)
                );
                //заокруглений
                robe.body_bottom = front.set().push(
                    front.path(kardiganFace[7]).attr(defaultPartBackground),
                    front.path(kardiganFace[7]).attr(defaultStrokeStyle),
                    back.path(kardiganBack[2]).attr(defaultPartBackground),
                    back.path(kardiganBack[2]).attr(defaultStrokeStyle),
                    front.image("http://knitel.com.ua/img/constructor/model-front-legs-short.png", 124, 414, 111, 272),
                    front.text(135, 378, '8').attr(fontStyle),
                    back.text(115, 250, '8').attr(fontStyle)
                );

                transform();
                iterateCollection();
                break;
        }
    });

    var detectIE = function() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // IE 12 => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }

    $("#order").on('click', function(){
        $("#order-info").val($.param(config));
        var isiOSSafari = (navigator.userAgent.match(/like Mac OS X/i)) ? true: false;
        var frontSVG = $('#front').html();

        var empty = true;
        for(var i in robe){
            if (

                (!_.isNull(robe[i][0].attrs) &&
                robe[i][0].attrs.fill == '#fff') ||
                (!_.isNull(robe[i][1].attrs) &&
                robe[i][1].attrs.fill == 'none')

            ) {
                empty = false;
            }
        }
        if (empty) {
            setTimeout(function(){
                if (detectIE() || isiOSSafari) {
                    config.image = frontSVG;
                    setTimeout(function(){
                        console.log(config);
                        $("input[name='photo']" ).val(config.image);

                        $("input[name='price']").val($("#order-price").text());
                        $("input[name='izdelee']" ).val($("#order-type").text());

                        //$('#order_popup').bPopup();
                        $('#order_popup').modal('show');
                    }, 200);
                } else {
                    var canvasID = "myCanvas";
                    var canvas = document.createElement('canvas');
                    canvas.id = canvasID;
                    canvas.setAttribute("style","display:none");
                    document.body.appendChild(canvas);
                    canvg(document.getElementById(canvasID), frontSVG, {
                        //renderCallback: function () {
                        //config.image = canvas.toDataURL("image/jpeg").toString();
                        //console.log(config.image);
                        //var img = document.createElement('img');
                        //img.src=config.image;
                        //$('body').append(img);
                        //}
                    });
                    setTimeout(function(){
                        //config.image = canvas.toDataURL("image/jpeg").toString();
                        config.image = (document.getElementById(canvasID)).toDataURL("image/jpeg", 1.0);

                        //var img = document.createElement('img');
                        //img.src=config.image;
                        //$('body').append(img);
                        //console.log(config);
                        $("input[name='photo']" ).val(config.image);

                        $("input[name='price']").val($("#order-price").text());
                        $("input[name='izdelee']" ).val($("#order-type").text());

                        //$('#order_popup').bPopup();
                        //$("html, body").animate({ scrollTop: 90 }, "slow");
                        $('#order_popup').modal('show');
                    }, 200);
                }
            }, 200);
        } else {
            $('#order_error').modal('show');
        }
    });
});